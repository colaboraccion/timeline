// const helper = require("../helper/jwt");
const mysql = require('../services/mysql');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        const listaTimeline = await mysql.query(`select * from testing.timeline`);
        return res.status(200).send(camelcaseKeys(listaTimeline));
        // return res.status(200).send("asd")
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
