require('dotenv').config();

const express = require('express');
const app = express();
const cors = require('cors');
/*const auth = require("./middleware/auth");
const users = require("./api_modules/users");*/
const timeline = require("./api_modules/timeline");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.options('*', cors());

app.get('/', (req, res) => { res.send('REST API para plataforma web') });
app.get('/api/timeline', timeline.getAll);

const port = process.env.APP_PORT;

app.listen(port)
console.log('app running on port', port);