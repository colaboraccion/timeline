const env = process.env;

const config = {
    db_pg: {
        host: env.PG_HOST || 'localhost',
        port: env.PG_PORT || '3306',
        user: env.PG_USER || 'roote',
        password: env.PG_PASSWORD || 'root',
        database: env.PG_NAME || 'testing',
    },
};

module.exports = config;