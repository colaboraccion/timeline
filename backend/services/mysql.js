const mysql  = require('mysql2');
const config = require('../config/db');
const pool = mysql.createPool(config.db_pg);
const promisePool = pool.promise();

async function query(query, params) {
    const [rows,fields]  = await promisePool.query(query, params);

    return rows;
}

module.exports = {
    query,
    pool
}