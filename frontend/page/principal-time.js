import { LitElement, html, css } from "lit-element";
import { tableStyles } from '../components-time/table-styles';
import { SelecttTime } from "../components-time/selectt-time";

export class PrincipalTime extends LitElement {
    static get properties() {
        return {
            data: { type: Array },
        };
    }
    createRenderRoot() {
        return this;
    }
    static get styles() {
        return [
            tableStyles,

        ]
    }

    async firstUpdated() {
        this.data = await this.getDATA();
    }

    async getDATA(where = '') {
        const req1 = await axios.get("http://localhost:3005/api/timeline");
        return req1.data;
    }
    // async getDepartamentos(where = '') {
    //     const req1 = await axios.get("http://78.46.16.8:5006/api/departamento", {
    //         params: {
    //             query1: ` 1=1 `
    //         }
    //     });
    //     return req1.data.dato;
    // }

    // async getSubTemas(where = '') {
    //     const req1 = await axios.get("http://78.46.16.8:5006/api/subtema", {
    //         params: {
    //             query1: ` 1=1 `
    //         }
    //     });
    //     return req1.data.dato;
    // }

    // async getData(where = '') {
    //     const req1 = await axios.get("http://78.46.16.8:5006/api/plan", {
    //         params: {
    //             query1: ` ${where}`
    //         }
    //     });
    //     console.log("e", req1.data.dato)
    //     return req1.data.dato;
    // }

    // async handleChangeSubTemas(event) {
    //     const detail = event.detail;
    //     this.subTemasElegidos = event.detail;
    //     console.log('subtemas:', event);
    //     this.data = await this.getData(detail)
    // }

    // async handleChangeDepartamentos(event) {
    //     const detail = event.detail;
    //     this.departamentos = event.detail;
    //     console.log('departamentos:', event);
    //     this.data = await this.getData(detail)
    // }

    constructor() {
        super();
        this.data = [];
    }

    render() {
        return html`  
        <div class="container">
        <br />
        <h3 align="center"><a href="">Demostracion Linea de tiempo</a></a></h3><br />
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Linea documentaria</h3>
                </div>
                <div class="panel-body">
                    <div class="timeline">
                        <div class="timeline__wrap">
                            <div class="timeline__items">
                                ${this.data.map(a => html`
                                    <div class="timeline__item">
                                        <div class="timeline__content">
                                            <h2>${a.titulo}</h2>
                                            <p>${a.comment}</p>
                                        </div>
                                    </div>
                                `)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    }
}

customElements.define('principal-time', PrincipalTime);