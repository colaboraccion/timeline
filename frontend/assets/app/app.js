var App = function () {

    let init = () => {
        html=``;
        getTime();       

    }

    //modularizar
    let getTime = async () => {        
        legendDates = await getTimeline();
        html+=`<div class="panel-body" >`;
        html+=`<div class="timeline">`;
        html+=`<div class="timeline__wrap">`;
        html+=`<div class="timeline__items">`;
        html+=`<div class="timeline__item">`;
            legendDates.data.forEach(element => {
                html+=`<div class="timeline__content">`;
                html+=`<h2>${element.comment}`;
                html+=`</h2>`;
                html+=`</div>`;
            });
        html+=`</div>`;
        html+=`</div>`;
        html+=`</div>`;
        html+=`</div>`;
        html+=`</div>`;
        document.getElementById(`time`).innerHTML=html;

    }
    let getTimeline = async () => {
        const res = await axios.get(`http://localhost:3005/api/timeline`);
        return res;
    }
    return {
        init: function () {
            return init();
        }
    }
}();
