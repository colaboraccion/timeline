import http from "./http-common"

const getAll = async () => {
    return http.get("/timeline");
};

const TimelineService = {
    getAll,
};

export default TimelineService;