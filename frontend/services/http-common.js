import axios from "axios";

export const BASE_URL_API_WEB = 'http://localhost:3005/api';

export default axios.create({
    baseURL: BASE_URL_API_WEB,
    headers: {
        'Content-type': 'application/json'
    }
});