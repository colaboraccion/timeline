import { LitElement, html, css } from "lit-element";

export class SelecttTime extends LitElement {

    static get properties() {
        return {
            identifier: { type: String },
            isOptionGroup: { type: Boolean },
            columnNameOptionGroup: { type: String },
            columnNameValue: { type: String },
            columnNameLabel: { type: String },
            itemsData: { type: Array },
            selectedItems: { type: Array },
        };
    }

    createRenderRoot() {
        return this;
    }

    updated() {
        if (this.identifier.length > 0) {
            $(`#${this.identifier}`).selectpicker('refresh');
        }
    }

    handleChange() {
        console.log( $(`#${this.identifier}`).val())
        this.selectedItems = $(`#${this.identifier}`).val().map((i) => Number(i))
        this.dispatchEvent(new CustomEvent('event-onchange', {
            detail: $(`#${this.identifier}`).val()
        }));
    }

    getOptionsGroup(itemOptsGroup = []) {
            if (itemOptsGroup.length > 0) {
                return html `
            ${itemOptsGroup && itemOptsGroup.map((itemOptGroup) => {
                return html`
                <optgroup label="${itemOptGroup[this.columnNameOptionGroup]}">
                    ${this.getOptions(itemOptGroup.data)}
                </optgroup>`;
            })}`;
        }
    }

    getOptions(itemOpts = []) {
        if (itemOpts.length > 0) {
            return html`
            ${itemOpts && itemOpts.map((itemOpt) => {
                return html`<option value="${itemOpt[this.columnNameValue]}">${itemOpt[this.columnNameLabel]}</option>`;
            })}`;
        }
    }

    constructor() {
        super();
        this.identifier = '';
        this.isOptionGroup = false;
        this.columnNameOptionGroup = '';
        this.columnNameValue = 'id';
        this.columnNameLabel = 'name';
        this.itemsData = [];
        this.selectedItems = [];
    }

    render() {
        return html`        
        <select id="${this.identifier}" class="selectpicker" multiple data-live-search="true" title="Seleccione..."
            data-actions-box="true" data-size="10" @change="${this.handleChange}">
            ${this.isOptionGroup ? 
            html `${this.getOptionsGroup(this.itemsData)}` : 
            html `${this.getOptions(this.itemsData)}`}
        </select>`;
    }
}

customElements.define('selectt-time', SelecttTime);